import java.util.Scanner;

class Addition {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) { //Methodenkopf
    	//public = Zugriffsmodifizierer
    	   double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;	 

        //1.Programmhinweis
        programmhinweis (); //Methodenaufruf
        //4.Eingabe		
      
       zahl1 = eingabe(1);	//Parameter
       zahl2 = eingabe(2);	//Parameter
       
        //3.Verarbeitung
       erg = verarbeitung (erg, zahl1, zahl2);//Argument

        //2.Ausgabe
        ausgabe (erg, zahl1, zahl2);
    }
        //Zeile 7 bis 23 Methodendefinition
    
    
    public static void programmhinweis (){	//Methodenbezeichner
    System.out.println("Hinweis: ");
    System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
    }
    
    public static void ausgabe (double erg, double zahl1, double zahl2) {
    	System.out.println("Die Rechnung der Addition lautet");
        System.out.printf("%.2f+%.2f = %.2f", zahl1, zahl2, erg);
     
        
    }
    
    public static double verarbeitung (double erg, double zahl1, double zahl2) {	//Parameter
    
    erg = zahl1 + zahl2;
    	return erg;										//R�ckgabewert/R�ckgabedatentyp
    }
    public static double eingabe (int z) {				//Lokale Variable
    double zahl1;
    	System.out.println(z +". Zahl:");
    zahl1 = sc.nextDouble();
    
    return zahl1; 
    
    }
    
    }
    
    
