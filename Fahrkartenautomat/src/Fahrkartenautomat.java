﻿import java.util.Scanner;

class Fahrkartenautomat
{
	
	public static double fahrkartenbestellungErfassen (Scanner tastatur) {
		
		
	    double zuZahlenderBetrag;
	    byte anzahlTickets;
	    
		
		System.out.println("Ticket Preis: ") ;
		zuZahlenderBetrag = tastatur.nextDouble();
	       System.out.println("Wie viele Tickets möchten Sie erwerben? ");
	       anzahlTickets = tastatur.nextByte();
	       
	       System.out.printf("Zu zahlender Betrag: %.2f EURO ", ( zuZahlenderBetrag * anzahlTickets ));
	       
	       return zuZahlenderBetrag * anzahlTickets;
	}
	       public static double fahrkartenBezahlen (double zuZahlenderBetrag, Scanner tastatur) {
	    	   
	    	   double eingezahlterGesamtbetrag;
	    	   double eingeworfeneMünze;
	    	   
	    	   
	    	   eingezahlterGesamtbetrag = 0.0f;
	           while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	           {
	        	   System.out.printf("Noch zu zahlen: %.2f EURO ", (zuZahlenderBetrag - eingezahlterGesamtbetrag ));
	        	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	        	   eingeworfeneMünze = tastatur.nextDouble();
	               eingezahlterGesamtbetrag += eingeworfeneMünze;
	           }
	    	   
	    	  return eingezahlterGesamtbetrag - zuZahlenderBetrag ;
	    	   
	       
	       
	}
	
	       public static void fahrkartenAusgabe() {
	    	   
	    	   System.out.println("\nFahrschein wird ausgegeben");
	           for (int i = 0; i < 25; i++)
	           {
	              System.out.print("=");
	              try {
	    			Thread.sleep(60);
	    		} catch (InterruptedException e) {
	    		
	    			e.printStackTrace();
	    		}
	           }
	           System.out.println("\n\n");
	    	   
	       }
	       
	       public static void rueckgeldAusgaben(double rückgabebetrag ) {
	    	   
	    	  
	    	   
	    	   if(rückgabebetrag > 0.00)
	           {
	        	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rückgabebetrag );
	        	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	               while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
	               {
	            	  System.out.println("2 EURO");
	    	          rückgabebetrag -= 2.00;
	               }
	               while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
	               {
	            	  System.out.println("1 EURO");
	    	          rückgabebetrag -= 1.00;
	               }
	               while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
	               {
	            	  System.out.println("50 CENT");
	    	          rückgabebetrag -= 0.50;
	               }
	               while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
	               {
	            	  System.out.println("20 CENT");
	     	          rückgabebetrag -= 0.20;
	               }
	               while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
	               {
	            	  System.out.println("10 CENT");
	    	          rückgabebetrag -= 0.10;
	               }
	               while(rückgabebetrag >= 0.04)// 5 CENT-Münzen
	               {
	            	  System.out.println("5 CENT");
	     	          rückgabebetrag -= 0.05;
	               }
	           }

	           System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                              "vor Fahrtantritt entwerten zu lassen!\n"+
	                              "Wir wünschen Ihnen eine gute Fahrt.");
	        }
	    	   
	       
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);      
      
       
       double zuZahlenderBetrag;  
       double rückgabebetrag;
       
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
       fahrkartenAusgabe();
       rueckgeldAusgaben(rückgabebetrag);

     

      
       
}}